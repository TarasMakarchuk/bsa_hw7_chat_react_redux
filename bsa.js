import Chat from './src/components/Chat/Chat';
import rootReducer from './src/reducers/rootReducer';

export default {
	Chat,
	rootReducer,
};
