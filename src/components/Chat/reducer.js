import {
	ADD_MESSAGE,
	UPDATE_MESSAGE,
	DELETE_MESSAGE,
	ADD_ALL_MESSAGES,
	SHOW_PRELOADER,
	SHOW_MODAL,
	ADD_TEMPORARY_MESSAGE_DATA
} from './actionTypes';

const initialState = {
	chat: {
		messages: [
			{
				id: "80f08600-1b8f-11e8-9629-c7eca82aa7bd",
				userId: "9e243930-83c9-11e9-8e0c-8f1a686f4ce4",
				avatar: "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
				user: "Ruth",
				text: "Hello *********. It's the Panama accounts",
				createdAt: "2020-07-16T19:48:12.936Z",
				editedAt: ""
			},
			{
				avatar: "",
				createdAt: "2021-07-09T12:48:10.437Z",
				editedAt: "",
				id: "80f08600-1b8f-11e8-9629-1625834890437",
				text: "Hello from redux!",
				user: "John",
				userId: "9999-8600-1b8f-11e8-9629-c7eca82a-9999",
			}
		],
		editModal: false,
		preloader: true,
	}
}

export default function chatReducer(state = initialState, action) {
	switch (action.type) {

		case ADD_ALL_MESSAGES: {
			const {data} = action.payload;
			return {
				...state.chat,
				messages: [...state.chat.messages, ...data]
			};
		}

		case SHOW_PRELOADER: {
			const {data} = action.payload;
			return {
				...state,
				preloader: data,
			};
		}

		case SHOW_MODAL: {
			return {
				...state,
				editModal: !state.editModal,
			};
		}

		case ADD_MESSAGE: {
			const {id, data} = action.payload;
			const newMessage = {id, ...data};

			return {
				...state.chat,
				messages: [...state.messages, newMessage]
			};
		}

		case ADD_TEMPORARY_MESSAGE_DATA: {
			const {id, data, index} = action.payload;

			return {
				...state,
				temporaryMessageData: {
					id,
					text: data,
					index
				},
			};
		}

		case UPDATE_MESSAGE: {
			const {id, data} = action.payload;

			const updateMessages = [];
			updateMessages.push(...state.messages);
			updateMessages.map(message => (message.id === id) ? message.text = data : message.text);

			return {
				...state.chat,
				messages: [...updateMessages]
			};

		}

		case DELETE_MESSAGE: {
			const {id} = action.payload;
			return {
				...state.chat,
				messages: [...state.messages.filter(message => message.id !== id)]
			};
		}

		default:
			return state;
	}
}