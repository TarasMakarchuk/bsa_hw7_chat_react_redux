FROM node

COPY public ./public
COPY src ./src
COPY .env ./
COPY package*.json ./
RUN yarn install

CMD ["yarn", "start"]

EXPOSE 80


