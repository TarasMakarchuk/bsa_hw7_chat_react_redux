import React, {Component} from 'react';
import Header from "../Header/Header";
import MessageList from "../MessageList/MessageList";
import './chat.css';
import logoImg from '../../assets/img/logo.png';
import MessageInput from "../MessageInput/MessageInput";
import Footer from "../Footer/Footer";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import * as actions from './actions';
import ModalDialog from "../ModalDialog/ModalDialog";
import store from "../../store/store";

class Chat extends Component {
	constructor(props) {
		super(props);
		this.state = {
			messages: null,
			text: '',
			preloader: true,
			ownMessages: [
				{
					id: "80f08600-1b8f-11e8-9629-c7eca82aa987",
					userId: "99998600-1b8f-11e8-9629-c7eca82a9999",
					avatar: '',
					user: "Bob",
					text: "I say hello!",
					createdAt: '2020-07-16T19:58:12.936Z',
					editedAt: "",
					isOwner: true,
				},
			],
		};
	}

	componentDidMount() {
		fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
			.then(response => response.json())
			.then(data => {
				if (data) {
					store.dispatch(actions.addAllMessages(data));
					setTimeout(() => {
						store.dispatch(actions.showPreloader(false))
					}, 1);
				}
			})
			.catch(error => console.error(error));
	}

	addMessage(message) {
		store.dispatch(actions.addMessage(message));
	}

	onChangeMessage(id, text) {
		store.dispatch(actions.showModal());
		const index = this.props.chatData.chat.messages.findIndex(item => item.id === id);
		if (id && text && index) {
			store.dispatch(actions.addTemporaryMessageData(id, text, index));
		}
	}

	onDeleteMessage(id) {
		store.dispatch(actions.deleteMessage(id));
	}

	render() {
		let isEditModal = this.props.chatData.chat.editModal;

		return (
			<div className='chat-container'>

				<div className='logo-container'>
					<img src={logoImg} alt="chat logo"/>
				</div>

				<div className='header-container'>
					<Header/>
				</div>

				<div className='content-container'>
					{isEditModal &&
					<ModalDialog onChangeMessage={() => this.onChangeMessage()}/>
					}
					<MessageList
						onDeleteMessage={(id) => this.onDeleteMessage(id)}
						onChangeMessage={(id, text) => this.onChangeMessage(id, text)}
					/>
					<MessageInput
						messages={this.state.messages}
						addMessage={(message) => this.addMessage(message)}
						handleSendOwnMessage={() => this.handleSendOwnMessage()}
						handleChangeInput={(event) => this.handleChangeInput(event)}
						inputMessage={this.state.text}
					/>
				</div>

				<div className='footer-container'>
					<Footer/>
				</div>

			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		chatData: state
	}
}

const mapDispatchToProps = {
	...actions
}

Chat.propTypes = {
	messages: PropTypes.arrayOf(
		PropTypes.shape({
			avatar: PropTypes.string,
			createdAt: PropTypes.string,
			editedAt: PropTypes.string,
			id: PropTypes.string,
			text: PropTypes.string,
			user: PropTypes.string,
			userId: PropTypes.string,
		})
	)
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);