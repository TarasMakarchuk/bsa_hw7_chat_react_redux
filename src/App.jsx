import React from 'react';
import Chat from "./components/Chat/Chat";
import './index.css';
import ErrorBoundary from "./error/ErrorBoundary";

function App() {
	return (
		<div className="App">
			<ErrorBoundary>
				<Chat/>
			</ErrorBoundary>
		</div>
	);
}

export default App;
