import React, {Component} from 'react';
import './header.css';
import Preloader from "../Preloader/Preloader";
import Moment from 'react-moment';
import moment from 'moment';
import * as actions from "../Chat/actions";
import {connect} from "react-redux";

class Header extends Component {
	constructor(props) {
		super(props);
		this.props = props;
	}

	render() {
		const {preloader} = this.props.chatData.chat;

		let messages = [];
		if (this.props.chatData.chat.messages) {
			messages.push(...this.props.chatData.chat.messages)
		}

		let lastDate = '';
		let participants = [];

		if (messages !== null) {
			const moments = messages.map(item => moment(item.createdAt));
			lastDate = moment.max(moments);
			messages.forEach(item => participants.push(item.userId));
			participants = [...new Set(participants)];
		}

		return (
			<div className='header'>
				<div className='header-info-block'>

					<div className='header-title'>
						<span>My chat</span>
					</div>

					<div className='header-users-count'>
						<span>{participants.length} participants</span>
					</div>

					<div className='header-messages-count'>
						<div>
							{preloader ?
								<Preloader/> :
								messages.length
							}
						</div>
						<div>
							<span>messages</span>
						</div>
					</div>
				</div>

				<div className='header-last-message-block'>
					<div className='header-last-message-date'>
						<span>Last message at</span>
						<div>
							{preloader ?
								<Preloader/> :
								<Moment
									format="DD.MM.YYYY HH:mm"
								>
									{lastDate}
								</Moment>
							}
						</div>
					</div>
				</div>

			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		chatData: state
	}
}

const mapDispatchToProps = {
	...actions
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);