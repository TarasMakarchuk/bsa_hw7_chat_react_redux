import React from 'react';
import * as actions from '../Chat/actions';
import store from "../../store/store";

import './modalDialog.css';
import {connect} from "react-redux";

class ModalDialog extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			text: this.props.chatData.chat.temporaryMessageData.text
		};
	}

	onClose() {
		store.dispatch(actions.showModal());
	};

	onSave() {
		const text = this.state.text;
		const id = this.props.chatData.chat.temporaryMessageData.id;
		store.dispatch(actions.updateMessage(id, text));
		console.log('Modal text  =============================================');
		console.log(text);
		this.setState({
			text: ''
		})

	}

	onChange(event) {
		this.setState({
			text: ''
		})
		const text = event.target.value;
		console.log(text);
		if (text !== '') {
			this.setState({
				text: text
			})
		}
	}

	render() {
		let isEditModal = this.props.chatData.chat.editModal;
		console.log('isEditModal');
		console.log(isEditModal);
		const showModalStyle = isEditModal ? 'modal-shown ' : '';

		return (
			<div className={`edit-message-modal ${showModalStyle}`}>
				<div className='edit-message-container'>
					<h2>Edit message</h2>
					<textarea
						onChange={(event) => this.onChange(event)}
						className='edit-message-input' cols="100" rows="4"
						value={this.state.text}
					/>
					<div className='edit-message-buttons-block'>
						<button className='edit-message-close' onClick={() => this.onClose()}>Close</button>
						<button className='edit-message-button' onClick={() => this.onSave()}>Save</button>
					</div>

				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		chatData: state
	}
}

const mapDispatchToProps = {
	...actions
}
export default connect(mapStateToProps, mapDispatchToProps)(ModalDialog);