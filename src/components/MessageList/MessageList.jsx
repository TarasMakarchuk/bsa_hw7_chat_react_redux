import React from 'react';
import Message from "./Message/Message";
import OwnMessage from "./OwnMessage/OwnMessage";
import Preloader from "../Preloader/Preloader";
import './messageList.css';
import * as actions from "../Chat/actions";
import {connect} from "react-redux";

class MessageList extends React.Component {
	constructor(props) {
		super(props);
		this.props = props;
	}

	render() {
		const {preloader} = this.props.chatData.chat;
		const ownerUserId = '9999-8600-1b8f-11e8-9629-c7eca82a-9999';

		let messages = [];
		if (this.props.chatData.chat.messages) {
			this.props.chatData.chat.messages.forEach((item, index) => {
				if (item.userId !== ownerUserId) {
					messages.push(item);
				}
			})
		}

		let ownMessages = [];
		if (this.props.chatData.chat.messages) {
			this.props.chatData.chat.messages.forEach((item, index) => {
				if (item.userId === ownerUserId) {
					ownMessages.push(item);
				}
			})
		}

		messages.sort((first, second) => new Date(first.createdAt) - new Date(second.createdAt));
		ownMessages.sort((first, second) => new Date(first.createdAt) - new Date(second.createdAt));

		return (
			<div className='message-list'>
				{preloader ?
					<Preloader/> :
					<div>
						{messages.map(item => <Message
							key={Date.now() + Math.random()}
							message={item}
						/>)}
						<hr className='messages-divider'/>
						{ownMessages.map(item => <OwnMessage
								key={Date.now() + Math.random()}
								ownMessages={item}
								onDeleteMessage={(id) => this.props.onDeleteMessage(id)}
								onChangeMessage={(id, text) => this.props.onChangeMessage(id, text)}
							/>
						)}
						<input ref={input => input && input.focus()}/>
					</div>
				}

			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		chatData: state
	}
}

const mapDispatchToProps = {
	...actions
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);